﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GerenciadorDoGame : MonoBehaviour {

	public static int numeroTotalDeBlocos;
	public static int numeroDeBlocosDestruidos;
	public Image estrelas;
	public GameObject canvasGo;
	public static GerenciadorDoGame instancia;
	public GameObject bola;
	public Plataforma plataforma;

	void Awake(){
		instancia = this;
	}

	void Start(){
		if (Application.loadedLevel == 1) {
			canvasGo.SetActive (false);
			numeroDeBlocosDestruidos = 0;
		}
	}

	public void FinalizarJogo(){
		plataforma.enabled = false;
		Destroy(bola.gameObject);
		estrelas.fillAmount = (float)numeroDeBlocosDestruidos / (float)numeroTotalDeBlocos;
		canvasGo.SetActive (true);
	}

	public void AlterarCena(string cena){
		Application.LoadLevel (cena);
	}

	public void FecharAplicativo(){
		Application.Quit ();
	}
}
