﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bola : MonoBehaviour {

	public Vector3 Direção;
	public float Velocidade;
	public GameObject particulaBlocos;
	public ParticleSystem particulaFolhas;
	public LineRenderer guia;
	public int pontosDaGuia = 3;

	// Use this for initialization
	void Start () {
		Direção.Normalize (); //mesmo que Direção = Dirẽção.normalized;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += Direção * Velocidade * Time.deltaTime;
		AtualizarLineRenderer ();
	}

	void AtualizarLineRenderer(){
		int pontoAtual = 1;
		Vector3 direçãoAtual = Direção;
		Vector3 ultimaPosição = transform.position;
		guia.SetPosition (0, ultimaPosição);
		while (pontoAtual < pontosDaGuia) {
			RaycastHit2D hit = Physics2D.Raycast (ultimaPosição, direçãoAtual);
			ultimaPosição = hit.point;
			guia.SetPosition (pontoAtual, ultimaPosição);
			direçãoAtual = Vector3.Reflect (direçãoAtual, hit.normal);
			ultimaPosição += direçãoAtual * 0.05F;
			pontoAtual++;
		}
	}

	void OnCollisionEnter2D (Collision2D colisor) {
		bool colisãoInvalida = false;
		Vector2 normal = colisor.contacts [0].normal;
		Plataforma plataforma = colisor.transform.GetComponent<Plataforma> ();
		GeradorDeArestas geradorDeArestas = colisor.transform.GetComponent<GeradorDeArestas> ();
		if (plataforma != null) {
			if (normal != Vector2.up) {
				colisãoInvalida = true;
			} else {
				particulaFolhas.transform.position = colisor.transform.position;
				particulaFolhas.Play ();
			}
		} else if (geradorDeArestas != null) {
			if (normal == Vector2.up) {
				colisãoInvalida = true;
			}
		} else {
			colisãoInvalida = false;
			Bounds bordasColisor = colisor.transform.GetComponent<SpriteRenderer> ().bounds;
			Vector3 posiçãoDaCriação = new Vector3 (colisor.transform.position.x + bordasColisor.extents.x, colisor.transform.position.y - bordasColisor.extents.y, colisor.transform.position.z);
			GameObject particulas = (GameObject)Instantiate (particulaBlocos, posiçãoDaCriação, Quaternion.identity);
			ParticleSystem componeneteParticulas = particulas.GetComponent<ParticleSystem> ();
			Destroy (particulas, componeneteParticulas.duration + componeneteParticulas.startLifetime);
			Destroy (colisor.gameObject);
			GerenciadorDoGame.numeroDeBlocosDestruidos++;
		}
		if (!colisãoInvalida) {
			Direção = Vector2.Reflect (Direção, normal);
			Direção.Normalize ();
		} else {
			GerenciadorDoGame.instancia.FinalizarJogo ();
		}
	}
}
